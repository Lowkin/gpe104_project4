﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicBox : MonoBehaviour {

    public static MusicBox mb; // our only music player the entire game

    private void Awake()
    {
        // If we don't have a GameManager instance set our singleton to this instance
        if (mb == null)
            mb = this;
        // Otherwise delete this one
        else
            Destroy(this.gameObject);
    }
    // Use this for initialization
    void Start () {
        DontDestroyOnLoad(this.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
