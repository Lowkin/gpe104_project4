﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : Controller {
    
    private enum States { Idle, Patrol}; // Variable to hold the various states our pawn can have
    private States currentState; // Variable to hold the current state of our pawn
    private int currentDestination; // Variable to hold the current waypoint we are moving to
    private int direction = 1; // Variable to indicate if we are moving forward or backwards through our waypoints
    private EnemyPawn enemyPawn; // EnemyPawn variable to hold reference to our current pawn

    public GameObject[] wayPoints; // List of empty gameobjects used for patrolling enemy waypoints
    public bool loop = false; // Variable to indicate if the AI should loop its route or go from one end to the other
    public float continueDistance; // Variable to hold how close the pawn needs to be to the current destination before continuing on
    public float maxWaitTime = 0; // Variable to hold how long we will wait between patrol points

    // Use this for initialization
    public override void Start () {
        base.Start();
        currentDestination = 0; // Start by going to the first patrol point
        currentState = States.Patrol; // set our starting state to patrol
        enemyPawn = currentPawn as EnemyPawn; // set our current pawn reference
        StartCoroutine("EnemyAIFSM"); // start our enemy AI state machine
    }
	
	// Update is called once per frame
	void Update () {

	}

    // FSM to control how our enemy AI works
    private IEnumerator EnemyAIFSM() 
    {
        while (true)
        {
            // If our current state is idle we sit idly by unless we hear something
            if (currentState == States.Idle)
            {
                enemyPawn.isMoving = false; // set our pawn to a non moving state
                float wait = 0; // current amount of time paused
                float waitTime = Random.Range(0, maxWaitTime); // max amount of time paused

                // while we have been waiting less time than the wait time
                while (wait < waitTime)
                {
                    wait += Time.deltaTime; // increment wait time
                    yield return null;
                }
                // set back to our patrol state
                currentState = States.Patrol; // set state back to patrol
            }
            // if our current state is patrol we patrol between our specified waypoints
            else if (currentState == States.Patrol)
            {
                enemyPawn.isMoving = true; // set our pawn to a moving state

                // if we do not have any waypoints (i.e. we are just a stationary guard
                if (wayPoints.Length == 0)
                {
                    yield return null;
                }
                // We check to see if we need to pick a new patrol point and direction
                else if (Mathf.Abs(enemyPawn.gameObject.transform.position.x - wayPoints[currentDestination].transform.position.x) <= continueDistance)
                //else if (Vector3.Distance(enemyPawn.gameObject.transform.position, wayPoints[currentDestination].transform.position) <= continueDistance)
                {
                    if (!loop)
                    {
                        // Check if going in the current direction is still possible 
                        if (direction + currentDestination < 0 || direction + currentDestination >= wayPoints.Length)
                            direction = -direction; // Flip our direction

                        // update our destination in the right direction
                        currentDestination += direction; // increment our index in our waypoint array
                    }
                    else
                    {
                        currentDestination = currentDestination == wayPoints.Length - 1 ? 0 : currentDestination + 1;
                    }
                    currentState = States.Idle; // swap to the idle state for a random amount of time before resuming patrol
                    yield return null;
                }
                // Otherwise we keep moving in the current direction
                else
                    enemyPawn.Move(wayPoints[currentDestination].transform.position - enemyPawn.gameObject.transform.position);

                yield return null;
            }
            yield return null;
        }
    }
}
