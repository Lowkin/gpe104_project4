﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pawn : MonoBehaviour {

    public float pawnSpeed;  // float to control this pawns speed

    public SpriteRenderer sr; // our sprite renderer reference

    public Animator animator; // animator reference

    [HideInInspector]
    public GameManager gm; // gm reference

    [HideInInspector]
    public bool isMoving = false; // bool to control if we are displaying idle or animation

    [HideInInspector]
    public Vector3 currentDirection; // Value to hold the direction we are currently facing

    [HideInInspector]
    public Rigidbody2D rb2d; // Variable to hold our rigidbody2d

    // Use this for initialization
    public virtual void Start () {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>() as GameManager; // get a reference to our game manager
        rb2d = GetComponent<Rigidbody2D>() as Rigidbody2D; // Get our rigidbody reference
        currentDirection = transform.right; // our current direction to start with is the way the sprite is facing
	}
	
	// Update is called once per frame
	public virtual void Update () {
        
    }

    // Base function to move this pawn via physics
    public virtual void Move(float speed)
    {
        rb2d.velocity = new Vector2(speed * pawnSpeed, rb2d.velocity.y);
        OrientDirection(new Vector3(rb2d.velocity.x, rb2d.velocity.y, 0f));
    }

    // Base function to move this pawn via transform positioning
    public virtual void Move(Vector3 direction)
    {
        currentDirection = new Vector3(direction.x, 0f, 0f).normalized; // set our pawns current direction to the direction we are wanting to face
        transform.position += (currentDirection * pawnSpeed * Time.deltaTime); // move our pawn in the provided direction
        OrientDirection(currentDirection); // point in the new direction
    }
    
    // Base function to make rigid body jump
    public virtual void Jump(float jumpForce)
    {
        rb2d.velocity = new Vector2(rb2d.velocity.x, 0f);
        rb2d.AddForce(Vector2.up * jumpForce);
    }

    // function to point our sprite in the direction we are moving
    public virtual void OrientDirection(Vector3 direction)
    {
        currentDirection = direction; // set our pawns current direction to the direction we are wanting to face
        ResetTriggers(); // set our movement direction triggers back to off

        // we are going right so set our sprite right
        if (direction.x > 0)
        {
            sr.flipX = false;
            animator.SetBool("movingRight", true);
        }
        // we are going left so set our sprite left
        else if (direction.x < 0)
        {
            sr.flipX = true;
            animator.SetBool("movingRight", true);
        }
        else
        {
            animator.SetBool("movingRight", false);
        }
    }

    // function to reset our animation movement triggers to false
    public void ResetTriggers()
    {
        animator.SetBool("movingRight", false);
    }
}
