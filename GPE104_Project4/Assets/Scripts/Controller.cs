﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Controller : MonoBehaviour {

    [HideInInspector]
    public Pawn currentPawn; // current pawn reference
    [HideInInspector]
    public GameManager gm; // gm reference

    // Use this for initialization
    public virtual void Start () {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>() as GameManager; // set our game manager reference
        currentPawn = transform.GetComponentInChildren<Pawn>() as Pawn;
    }
}
