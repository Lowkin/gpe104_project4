﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPawn : Pawn {

    //[HideInInspector]
    public bool isGrounded;
    // Use this for initialization
    public override void Start () {
        base.Start();
        gm.playerController.currentPawn = this; // get a reference to our controller when we are instantiated
        isGrounded = true; // start off gorunded
	}

    public override void Update()
    {
        base.Update();
        // if we arent moving we will want to set our movement triggers to off
        if (rb2d.velocity.magnitude <= 0)
            ResetTriggers(); // reset our movement triggers
        if (rb2d.velocity.y > 0)
            animator.SetBool("jumping", true); // we are jumping
        else
            animator.SetBool("jumping", false); // we are not jumping
    }

    // function to handle collisions
    public void OnCollisionEnter2D(Collision2D collision)
    {
        // here we deal with running into the enemy or falling out of bounds
        if (collision.gameObject.layer == gm.enemyLayer || collision.gameObject.layer == gm.outOfBoundsLayer)
        {
            gm.gameOver = true; // game is over
        }
    }

    // fuction to handle entering triggers
    public void OnTriggerEnter2D(Collider2D collision)
    {
        // here we deal with reaching the end of the level
        if (collision.gameObject.layer == gm.gameWinLayer)
        {
            gm.gameWon = true; // we win!
        }
    }
}
