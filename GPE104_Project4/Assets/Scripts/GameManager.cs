﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static GameManager gm; // Singleton GameManager object

    private int currentScore = 0; // players current score

    private float cameraYBorder = 0.45f; // the lowest the camera will go when following the player

    private UISettings uiSettings; // our UI settings that came through from the main menu

    public int startingLoot; // starting amount of loot we get
    public float maxJumps; // how many jumps can we do before landing again
    public float jumpForce; // how strong is our jump
    public Camera mainCamera; // reference for our camera
    public GameObject playerSpawnPoint; // player spawn location
    public GameObject gameOverUI; // Variable to store a reference to our game over menu
    public GameObject winUI; // Variable to store a reference to our win menu
    public GameObject gamePlayUI; // Variable to store a reference to our game play ui
    public Controller playerController; // Variable to hold a reference to our player controller
    public GameObject malePawn; // Variable to hold a reference to our male pawn
    public GameObject femalePawn; // Variable to hold a reference to our female pawn
    public GameObject cameraLeftBound; // Variable to hold a reference to the farthest left and down we want the camera to go
    public GameObject cameraRightBound; // Variable to hold a reference to the farthest right and down we want the camera to go
    public Text lootCount; // Variable to hold a reference to our loot UI

    public AudioClip gameOverSound; // Sound for game over
    public AudioClip pickUpSound; // sound for picking up a collectible
    public AudioClip loseLifeSound; // sound for hitting an enemy or falling out of the ring
    public AudioClip gameWonSound; // sound for winning the game

    public AudioSource gmAudioSource; // Reference to our audio source
    public AudioSource playerAudioSource; // Reference to our player audio source

    // declarations for our various layers
    [HideInInspector]
    public int playerLayer;
    [HideInInspector]
    public int enemyLayer;
    [HideInInspector]
    public int collectibleLayer;
    [HideInInspector]
    public int groundLayer;
    [HideInInspector]
    public int checkPointLayer;
    [HideInInspector]
    public int obstacleLayer;
    [HideInInspector]
    public int outOfBoundsLayer;
    [HideInInspector]
    public int gameWinLayer;
    [HideInInspector]
    public int wallLayer;

    [HideInInspector]
    public enum States { Active, GameOver }; // Variable to hold the various states our game
    [HideInInspector]
    public States currentState; // Variable to hold the current state of our pawn
    [HideInInspector]
    public bool gameOver = false; // trigger to switch to game over mode 
    public bool gameWon = false; // trigger to switch to game won mode
    public GameObject playerPawn; // reference to our player's current pawn

    void Awake()
    {
        // If we don't have a GameManager instance set our singleton to this instance
        if (gm == null)
            gm = this;
        // Otherwise delete this one
        else
            Destroy(this.gameObject);

        uiSettings = GameObject.Find("UISettings").transform.GetComponent<UISettings>() as UISettings; // get our UI settings from the main menu

        // Assign our layer variables
        playerLayer = LayerMask.NameToLayer("Player");
        enemyLayer = LayerMask.NameToLayer("Enemy");
        collectibleLayer = LayerMask.NameToLayer("Collectible");
        groundLayer = LayerMask.NameToLayer("Terrain");
        checkPointLayer = LayerMask.NameToLayer("CheckPoint");
        outOfBoundsLayer = LayerMask.NameToLayer("OutOfBounds");
        gameWinLayer = LayerMask.NameToLayer("WinGame");
        wallLayer = LayerMask.NameToLayer("Wall");

        lootCount.text = startingLoot.ToString(); // set our gameplay UI values

        currentScore = startingLoot; // set our starting loot

        currentState = States.Active; // set our state to active

        Physics2D.IgnoreLayerCollision(enemyLayer, enemyLayer); // dont care if enemies pass by eachother
    }

    // Use this for initialization
    void Start () {
        StartCoroutine("GameFSM"); // start our game state machine

        if (playerPawn == null)
        {
            // if we have selected male, instantiate a male pawn
            if (uiSettings.male)
                playerPawn = Instantiate(malePawn, playerSpawnPoint.transform.position, Quaternion.identity, playerController.transform);
            //if we have selected female, instantiate a female pawn
            else
                playerPawn = Instantiate(femalePawn, playerSpawnPoint.transform.position, Quaternion.identity, playerController.transform);
        }
    }
	
    // FSM to run our game 
    public IEnumerator GameFSM()
    {
        // we always want to be running this
        while (true)
        {
            // if we are in an active state
            if (currentState == States.Active)
            {
                gameOverUI.SetActive(false); // set our game over UI inactive
                gamePlayUI.SetActive(true); // set our gameplay UI active
                winUI.SetActive(false); // set our win ui inactive

                // if we have toggled to game over
                if (gameOver || gameWon)
                    currentState = States.GameOver; // if something triggered game over set our state

                // Set our Camera always on the player
                

                yield return null;
            }
            // if we are in a game over state
            else if (currentState == States.GameOver)
            {                
                // if we have loot and got to the end we win
                if (currentScore > 0 && gameWon)
                {
                    winUI.SetActive(true); // set our win ui inactive
                    gameOverUI.SetActive(false); // set our game over UI active
                    playerController.gameObject.SetActive(false);
                    AudioSource.PlayClipAtPoint(gameWonSound, playerPawn.transform.position); // play our clip
                }

                // if we have enough loot we can respawn
                else if (currentScore > 0 && gameOver)
                {
                    currentScore--;  //decrement our score
                    playerPawn.transform.position = playerSpawnPoint.transform.position; // move our player to the respawn point
                    lootCount.text = currentScore.ToString(); // update our UI to display our score
                    currentState = States.Active; // set our game state to active
                    AudioSource.PlayClipAtPoint(loseLifeSound, playerPawn.transform.position);// play our clip
                }

                // if we have no more loot left and we got hit or left
                else if (currentScore == 0 && (gameOver || gameWon))
                {
                    winUI.SetActive(false); // set our win ui inactive
                    gameOverUI.SetActive(true); // set our game over UI active
                    playerController.gameObject.SetActive(false);
                    gmAudioSource.clip = gameOverSound; // set our clip to the game over sound
                    gmAudioSource.Play(); // play our clip
                    gamePlayUI.SetActive(false); // set our gameplay UI inactive
                }

                // set our booleans back to false
                gameOver = false;
                gameWon = false;

                yield return null;
            }
            yield return null; // make sure the rest of our game can run its logic
        }
    }
    public void Collect()
    {
        currentScore++; // increment our loot count
        lootCount.text = currentScore.ToString(); // update our UI to display our score
        playerAudioSource.clip = pickUpSound;
        playerAudioSource.Play();
    }

    // function to handle quitting the application
    public void Quit()
    {
        Application.Quit(); // close the application
    }

    // function to return to main menu
    public void MainMenu()
    {
        Destroy(uiSettings.gameObject);
        SceneManager.LoadScene(0); // reload our game scene
    }

    // Set our camera position in late update so we know all calculations and positions have been updated
    public void LateUpdate()
    {
        // Set our camera to lerp to follow the player;  we lerp to avoid jagged movement
        if (gm.currentState == States.Active)
        {
            Vector3 newLocation = new Vector3(playerPawn.transform.position.x, playerPawn.transform.position.y, mainCamera.transform.position.z);
            if (newLocation.x < cameraLeftBound.transform.position.x)
                newLocation.x = cameraLeftBound.transform.position.x;
            else if (newLocation.x > cameraRightBound.transform.position.x)
                newLocation.x = cameraRightBound.transform.position.x;
            if (newLocation.y < cameraYBorder)
                newLocation.y = cameraYBorder;
            mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, newLocation, Time.deltaTime*5);
        }

        // if we hit escape go back to main menu
        if (Input.GetKeyDown(KeyCode.Escape))
            SceneManager.LoadScene(0); // reload our game scene
    }

    // set our spawn point to the last checkpoint
    public void UpdateSpawnPoint(GameObject newLocation)
    {
        playerSpawnPoint = newLocation;
    }
}
