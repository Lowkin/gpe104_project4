﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundedDetector : MonoBehaviour {

    public PlayerPawn pp; // reference to our player
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    // If we enter the ground we are grounded
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == pp.gm.groundLayer)
            pp.isGrounded = true; // we are grounded
    }
}
