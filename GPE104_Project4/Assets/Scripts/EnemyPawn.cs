﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class EnemyPawn : Pawn
{
    public override void Update()
    {
        base.Update();
        // if we arent moving we need to set our triggers for animation
        if (!isMoving)
            ResetTriggers();
    }
}
