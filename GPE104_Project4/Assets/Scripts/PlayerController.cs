﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Controller {

    private bool moving; // Variable to tell if the player controller is moving
    private int jumpCount; // Variable to hold how many jumps we have done

    // Use this for initialization
    public override void Start () {
        base.Start();
        jumpCount = 0;
    }
	
	// Update is called once per frame
	void Update () {

        // if we are in an active game state
        if (gm.currentState == GameManager.States.Active && currentPawn != null)
        {
            bool grounded = ((PlayerPawn)currentPawn).isGrounded;

            if (grounded)
            {
                float xMovement = Input.GetAxis("Horizontal"); // get our horizontal movement
                currentPawn.Move(xMovement); // move in the direction of our input axis
            }

            // if we are grounded and jump or if we jump while we still have max jumps
            if (Input.GetKeyDown(KeyCode.Space) && (((PlayerPawn)currentPawn).isGrounded || jumpCount < gm.maxJumps))
            {
                currentPawn.Jump(gm.jumpForce); // add our force
                jumpCount++; // Increment our jump counter
                ((PlayerPawn)currentPawn).isGrounded = false;
            }
            // if we are grounded we reset our jump counter
            if (((PlayerPawn)currentPawn).isGrounded == true)
                jumpCount = 0;
        }
    }
}
