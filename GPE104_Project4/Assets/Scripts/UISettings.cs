﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UISettings : MonoBehaviour {

    [HideInInspector]
    public bool male; // variable to hold the selected gender

    public Toggle maleOption; // Variable to hold a reference to our male gender selection
    public Toggle femaleOption; // Variable to hold a reference to our female gender selection

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject); // dont destroy our preferences
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}

    // function for our UI to load our game
    public void Play()
    {
        // check our toggles to set the gender
        if (femaleOption.isOn)
            male = false;
        else
            male = true;

        // load our gameplay scene
        SceneManager.LoadScene(1); // load our gameplay scene
    }

    // function for our UI to close the application
    public void Quit()
    {
        Application.Quit();
    }
}
